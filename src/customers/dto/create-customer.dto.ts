import { IsNotEmpty } from "class-validator";

export class CreateCustomerDto {
  @IsNotEmpty()
  id: number;
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  age: number;
  @IsNotEmpty()
  tel: string;
  @IsNotEmpty()
  gender: string;

}
